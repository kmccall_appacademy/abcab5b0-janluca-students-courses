require 'course.rb'

class Student
  attr_accessor :first_name, :last_name, :courses
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    @first_name + " " + @last_name
  end

  def enroll(new_course)
    courses.each do |course|
      if course.conflicts_with?(new_course)
        raise "Course #{new_course} conflicts with #{course}"
      end
    end

    if !@courses.include?(new_course)
      @courses << new_course
      new_course.students << self
    end
  end

  def course_load
    total_load = Hash.new(0)
    @courses.each do |course|
      total_load[course.department] += course.credits
    end
    total_load
  end

end
